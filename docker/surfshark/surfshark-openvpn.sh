#!/bin/sh

VPN_HOME=/vpn
VPN_CONFIG=${VPN_HOME}/config

add_lan_route() {
  network=$1
  gw="$(ip route |awk '/default/ {print $3}')"
  ip route | grep -q "$network" || ip route add to $network via $gw dev eth0
}

[ -d $VPN_CONFIG ] && rm -rf $VPN_CONFIG
mkdir -p $VPN_CONFIG

wget -O ovpn-config.zip https://my.surfshark.com/vpn/api/v1/server/configurations
unzip ovpn-config.zip -d $VPN_CONFIG

cd $VPN_CONFIG
VPN_FILE=$(ls -1 | grep "${VPN_TRANSPORT_TYPE}" | shuf | head -n 1)

echo "vpn profile: ${VPN_FILE}"

[[ -n ${LAN_NETWORK} ]] && for net in ${LAN_NETWORKS//[:;,]/ } ; do add_lan_route ${net} ; done


printf "${VPN_USER}\n${VPN_PASSWORD}\n" > $VPN_HOME/vpn-auth.txt
openvpn --config $VPN_CONFIG/$VPN_FILE --auth-user-pass $VPN_HOME/vpn-auth.txt
