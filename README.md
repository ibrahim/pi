# pi

setup my raspberry pi from scratch

## installed apps

- docker-ce (with buildx for `arm64` and `x86_64` builds)
- k3s
- helm
- goenv
- plex media server (todo)
- gitlab-runner (todo)

## k3s services

- deploy surfshark vpn + transmission torrent + pikms (todo)


## ansible

- install ansible
  ```shell
  sudo apt install ansible
  ```
- clone repo

- change to  ansible directory
  ```
  cd ansible  
  ```

- copy `default.config.yml` to `config.yml` and make local changes if required

- run ansible playbook 
  ```
  ansible-playbook setup.yml --ask-vault-password
  ```

## helm

install vpn chart

- change to helm directory
```
cd helm
```

- install helm vpn chart on `vpn` namespace using a release name of `pi4`
```
helm install -n vpn pi4 vpn/
```
